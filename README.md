# ESPIPMI #

This project gives you possibility to control your remote server by additional backdor device via wifi.

### How do I get set up? ###

* Follow [esp-open-rtos](https://github.com/SuperHouse/esp-open-rtos) project and prepare just esp-open-sdk in your desktop.
* Clone this repository and jump into `firmware`
* type `make` and wait till esp-open-rtos with libs be prepared.
* Update `esp-open-rtos/include/ssid_config.h` and put your own credentials to access point.
* type again `make` for final compilation.

### Network services ###
* port 23 - telnet port (used by uart bridge)

### RESULT :) ###

![IMG_20160517_164321_new.jpg](https://bitbucket.org/repo/xkb9n5/images/1482045272-IMG_20160517_164321_new.jpg)
![IMG_20160517_163632.jpg](https://bitbucket.org/repo/xkb9n5/images/449534990-IMG_20160517_163632.jpg)
![IMG_20160517_163531.jpg](https://bitbucket.org/repo/xkb9n5/images/2353844521-IMG_20160517_163531.jpg)
![IMG_20160517_163457.jpg](https://bitbucket.org/repo/xkb9n5/images/4203227739-IMG_20160517_163457.jpg)
![IMG_20160517_163436.jpg](https://bitbucket.org/repo/xkb9n5/images/3276520460-IMG_20160517_163436.jpg)