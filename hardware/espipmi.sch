EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:hetii
LIBS:espipmi-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP-07v2 U2
U 1 1 5739E871
P 5800 2250
F 0 "U2" H 5800 2150 50  0000 C CNN
F 1 "ESP-07v2" H 5800 2350 50  0000 C CNN
F 2 "hetii:ESP-07v2" H 5800 2250 50  0001 C CNN
F 3 "" H 5800 2250 50  0001 C CNN
	1    5800 2250
	1    0    0    -1  
$EndComp
$Comp
L MAX232 U1
U 1 1 5739E936
P 3025 2050
F 0 "U1" H 2575 2900 50  0000 L CNN
F 1 "MAX232" H 3225 2900 50  0000 L CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 3025 2050 50  0001 C CNN
F 3 "" H 3025 2050 50  0000 C CNN
	1    3025 2050
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5739EA63
P 3950 2100
F 0 "C5" H 3975 2200 50  0000 L CNN
F 1 "100nF" H 3700 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3988 1950 50  0001 C CNN
F 3 "" H 3950 2100 50  0000 C CNN
	1    3950 2100
	-1   0    0    1   
$EndComp
$Comp
L C C2
U 1 1 5739EBF0
P 2425 2050
F 0 "C2" H 2450 2150 50  0000 L CNN
F 1 "100nF" H 2450 1950 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2463 1900 50  0001 C CNN
F 3 "" H 2425 2050 50  0000 C CNN
	1    2425 2050
	-1   0    0    1   
$EndComp
$Comp
L C C4
U 1 1 5739EC43
P 3700 2100
F 0 "C4" H 3725 2200 50  0000 L CNN
F 1 "100nF" H 3675 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3738 1950 50  0001 C CNN
F 3 "" H 3700 2100 50  0000 C CNN
	1    3700 2100
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 5739ECB0
P 2425 1550
F 0 "C1" H 2450 1650 50  0000 L CNN
F 1 "100nF" H 2450 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2463 1400 50  0001 C CNN
F 3 "" H 2425 1550 50  0000 C CNN
	1    2425 1550
	-1   0    0    1   
$EndComp
$Comp
L C C3
U 1 1 5739F26D
P 3700 1500
F 0 "C3" H 3725 1600 50  0000 L CNN
F 1 "100nF" H 3725 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3738 1350 50  0001 C CNN
F 3 "" H 3700 1500 50  0000 C CNN
	1    3700 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5739F380
P 3825 2250
F 0 "#PWR01" H 3825 2000 50  0001 C CNN
F 1 "GND" H 3950 2200 50  0000 C CNN
F 2 "" H 3825 2250 50  0000 C CNN
F 3 "" H 3825 2250 50  0000 C CNN
	1    3825 2250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5739FA59
P 1525 2850
F 0 "#PWR02" H 1525 2600 50  0001 C CNN
F 1 "GND" H 1525 2700 50  0000 C CNN
F 2 "" H 1525 2850 50  0000 C CNN
F 3 "" H 1525 2850 50  0000 C CNN
	1    1525 2850
	1    0    0    -1  
$EndComp
Text Label 1525 2350 0    60   ~ 0
TXD
Text Label 1525 2550 0    60   ~ 0
DTR
Text Label 1525 2250 0    60   ~ 0
RTS
Text Label 1525 2150 0    60   ~ 0
RXD
$Comp
L R R5
U 1 1 5739FF46
P 4775 1500
F 0 "R5" V 4855 1500 50  0000 C CNN
F 1 "10k" V 4775 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4705 1500 50  0001 C CNN
F 3 "" H 4775 1500 50  0000 C CNN
	1    4775 1500
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 573A0174
P 6775 1500
F 0 "R7" V 6855 1500 50  0000 C CNN
F 1 "10k" V 6775 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6705 1500 50  0001 C CNN
F 3 "" H 6775 1500 50  0000 C CNN
	1    6775 1500
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 573A04FC
P 4600 1500
F 0 "R4" V 4680 1500 50  0000 C CNN
F 1 "10k" V 4600 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4530 1500 50  0001 C CNN
F 3 "" H 4600 1500 50  0000 C CNN
	1    4600 1500
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 573A0907
P 6850 2700
F 0 "R8" V 6930 2700 50  0000 C CNN
F 1 "10k" V 6850 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6780 2700 50  0001 C CNN
F 3 "" H 6850 2700 50  0000 C CNN
	1    6850 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 573A0C24
P 5800 3250
F 0 "#PWR03" H 5800 3000 50  0001 C CNN
F 1 "GND" H 5800 3100 50  0000 C CNN
F 2 "" H 5800 3250 50  0000 C CNN
F 3 "" H 5800 3250 50  0000 C CNN
	1    5800 3250
	1    0    0    -1  
$EndComp
Entry Wire Line
	1775 2350 1875 2250
Entry Wire Line
	1775 2250 1875 2150
Entry Wire Line
	1775 2150 1875 2050
Entry Wire Line
	1775 2550 1875 2450
NoConn ~ 1525 1950
NoConn ~ 1525 2050
NoConn ~ 1525 2450
NoConn ~ 1525 2650
Entry Wire Line
	2100 2450 2200 2550
Entry Wire Line
	2100 2550 2200 2650
Entry Wire Line
	2100 2650 2200 2750
Text Label 4075 2750 2    60   ~ 0
TXD
Text Label 4075 2550 2    60   ~ 0
RXD
Text Label 2425 2650 2    60   ~ 0
E_DTR
Entry Wire Line
	4075 2550 4175 2450
Entry Wire Line
	4075 2650 4175 2550
Entry Wire Line
	4075 2750 4175 2650
Text Label 2425 2550 2    60   ~ 0
E_TXD
Text Label 2425 2750 2    60   ~ 0
E_RXD
Text Label 4075 2650 2    60   ~ 0
DTR
NoConn ~ 2425 2450
NoConn ~ 3625 2450
NoConn ~ 4900 2050
NoConn ~ 6700 2150
NoConn ~ 6700 2250
Entry Wire Line
	7050 1950 7150 1850
Entry Wire Line
	7050 2050 7150 1950
Entry Wire Line
	7150 1950 7250 2050
$Comp
L GND #PWR04
U 1 1 573A3860
P 6850 2950
F 0 "#PWR04" H 6850 2700 50  0001 C CNN
F 1 "GND" H 6850 2800 50  0000 C CNN
F 2 "" H 6850 2950 50  0000 C CNN
F 3 "" H 6850 2950 50  0000 C CNN
	1    6850 2950
	1    0    0    -1  
$EndComp
Text Label 7050 1950 2    60   ~ 0
E_TXD
Text Label 7050 2050 2    60   ~ 0
E_RXD
Text Label 7475 2050 2    60   ~ 0
E_DTR
$Comp
L R R3
U 1 1 573A3CD4
P 4475 2100
F 0 "R3" V 4555 2100 50  0000 C CNN
F 1 "0" V 4475 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4405 2100 50  0001 C CNN
F 3 "" H 4475 2100 50  0000 C CNN
	1    4475 2100
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 573A3D87
P 4300 2100
F 0 "R2" V 4380 2100 50  0000 C CNN
F 1 "0" V 4300 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4230 2100 50  0001 C CNN
F 3 "" H 4300 2100 50  0000 C CNN
	1    4300 2100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 573A4432
P 9225 1400
F 0 "P3" H 9350 1475 50  0000 C CNN
F 1 "POWER_IN +5V" H 9575 1400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 9225 1400 50  0001 C CNN
F 3 "" H 9225 1400 50  0000 C CNN
	1    9225 1400
	1    0    0    -1  
$EndComp
$Comp
L NCP1117ST33T3G U3
U 1 1 573A4662
P 8250 1400
F 0 "U3" H 8250 1650 50  0000 C CNN
F 1 "NCP1117ST33T3G" H 8250 1600 50  0000 C CNN
F 2 "hetii:sot-223" H 8250 1400 50  0001 C CNN
F 3 "" H 8250 1400 50  0000 C CNN
	1    8250 1400
	-1   0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 573A470C
P 8850 1500
F 0 "C7" H 8875 1600 50  0000 L CNN
F 1 "100nF" H 8600 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8888 1350 50  0001 C CNN
F 3 "" H 8850 1500 50  0000 C CNN
	1    8850 1500
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 573A4781
P 7625 1500
F 0 "C6" H 7650 1600 50  0000 L CNN
F 1 "100nF" H 7650 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7663 1350 50  0001 C CNN
F 3 "" H 7625 1500 50  0000 C CNN
	1    7625 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 573A4CA0
P 9025 1725
F 0 "#PWR05" H 9025 1475 50  0001 C CNN
F 1 "GND" H 9100 1725 50  0000 C CNN
F 2 "" H 9025 1725 50  0000 C CNN
F 3 "" H 9025 1725 50  0000 C CNN
	1    9025 1725
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P4
U 1 1 573A4E6D
P 9225 2075
F 0 "P4" H 9350 2150 50  0000 C CNN
F 1 "MB_RESET" H 9500 2075 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 9225 2075 50  0001 C CNN
F 3 "" H 9225 2075 50  0000 C CNN
	1    9225 2075
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P5
U 1 1 573A4FEB
P 9225 2400
F 0 "P5" H 9350 2475 50  0000 C CNN
F 1 "MB_POWER" H 9500 2400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 9225 2400 50  0001 C CNN
F 3 "" H 9225 2400 50  0000 C CNN
	1    9225 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 573A5317
P 8925 2225
F 0 "#PWR06" H 8925 1975 50  0001 C CNN
F 1 "GND" H 9150 2225 50  0000 C CNN
F 2 "" H 8925 2225 50  0000 C CNN
F 3 "" H 8925 2225 50  0000 C CNN
	1    8925 2225
	1    0    0    -1  
$EndComp
$Comp
L BC817-40 Q1
U 1 1 573A5BE6
P 3375 3250
F 0 "Q1" H 3575 3325 50  0000 L CNN
F 1 "BC817-40" H 3575 3250 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3550 3025 50  0000 L CIN
F 3 "" H 3375 3250 50  0000 L CNN
	1    3375 3250
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 573A5E3D
P 2925 3250
F 0 "R1" V 3005 3250 50  0000 C CNN
F 1 "22k" V 2925 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 2855 3250 50  0001 C CNN
F 3 "" H 2925 3250 50  0000 C CNN
	1    2925 3250
	0    1    1    0   
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 573A5F32
P 4250 3250
F 0 "P1" H 4250 3400 50  0000 C CNN
F 1 "Z_RESET" V 4350 3250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 4250 3250 50  0001 C CNN
F 3 "" H 4250 3250 50  0000 C CNN
	1    4250 3250
	0    1    1    0   
$EndComp
$Comp
L GND #PWR07
U 1 1 573A6372
P 3475 3550
F 0 "#PWR07" H 3475 3300 50  0001 C CNN
F 1 "GND" H 3475 3400 50  0000 C CNN
F 2 "" H 3475 3550 50  0000 C CNN
F 3 "" H 3475 3550 50  0000 C CNN
	1    3475 3550
	1    0    0    -1  
$EndComp
Entry Wire Line
	2100 3150 2200 3250
Text Label 2200 3250 0    60   ~ 0
RTS
Text Label 4300 3050 1    60   ~ 0
E_RESET
$Comp
L CONN_01X02 P2
U 1 1 573A708B
P 7675 2100
F 0 "P2" H 7675 1950 50  0000 C CNN
F 1 "Z_BOOTLOADER" H 7475 2275 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 7675 2100 50  0001 C CNN
F 3 "" H 7675 2100 50  0000 C CNN
	1    7675 2100
	1    0    0    -1  
$EndComp
$Comp
L BC817-40 Q2
U 1 1 573A790B
P 8525 2225
F 0 "Q2" H 8725 2300 50  0000 L CNN
F 1 "BC817-40" H 8525 2475 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8400 2550 50  0000 L CIN
F 3 "" H 8525 2225 50  0000 L CNN
	1    8525 2225
	1    0    0    -1  
$EndComp
$Comp
L BC817-40 Q3
U 1 1 573A7AA5
P 8525 2750
F 0 "Q3" H 8725 2825 50  0000 L CNN
F 1 "BC817-40" H 8725 2750 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8725 2675 50  0000 L CIN
F 3 "" H 8525 2750 50  0000 L CNN
	1    8525 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 573A8012
P 8625 3050
F 0 "#PWR08" H 8625 2800 50  0001 C CNN
F 1 "GND" H 8625 2900 50  0000 C CNN
F 2 "" H 8625 3050 50  0000 C CNN
F 3 "" H 8625 3050 50  0000 C CNN
	1    8625 3050
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 573A826C
P 8175 2350
F 0 "R11" V 8255 2350 50  0000 C CNN
F 1 "1k" V 8175 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8105 2350 50  0001 C CNN
F 3 "" H 8175 2350 50  0000 C CNN
	1    8175 2350
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 573A82CF
P 8175 2525
F 0 "R12" V 8255 2525 50  0000 C CNN
F 1 "1k" V 8175 2525 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8105 2525 50  0001 C CNN
F 3 "" H 8175 2525 50  0000 C CNN
	1    8175 2525
	0    1    1    0   
$EndComp
Wire Wire Line
	2425 1350 2425 1400
Wire Wire Line
	2425 1700 2425 1750
Wire Wire Line
	2425 1850 2425 1900
Wire Wire Line
	2425 2200 2425 2250
Wire Wire Line
	3625 1950 3825 1950
Wire Wire Line
	3625 2250 3700 2250
Wire Wire Line
	3625 1750 3950 1750
Wire Wire Line
	3950 1750 3950 1950
Connection ~ 3700 1950
Wire Wire Line
	3625 1350 7850 1350
Wire Wire Line
	3700 1950 3700 1650
Wire Wire Line
	1525 2750 1525 2850
Connection ~ 4775 1350
Connection ~ 5800 1350
Wire Wire Line
	4775 1650 4775 1950
Wire Wire Line
	4475 1950 4900 1950
Wire Wire Line
	5800 3150 5800 3250
Connection ~ 4600 1350
Connection ~ 3700 1350
Wire Wire Line
	1525 2150 1775 2150
Wire Wire Line
	1525 2250 1775 2250
Wire Wire Line
	1525 2350 1775 2350
Wire Wire Line
	1525 2550 1775 2550
Wire Wire Line
	2425 2550 2200 2550
Wire Wire Line
	2425 2650 2200 2650
Wire Wire Line
	2425 2750 2200 2750
Wire Wire Line
	3825 1950 3825 2250
Wire Wire Line
	3825 2250 3950 2250
Wire Wire Line
	3625 2550 4075 2550
Wire Wire Line
	3625 2650 4075 2650
Wire Wire Line
	3625 2750 4075 2750
Wire Bus Line
	2100 1100 2100 3150
Wire Bus Line
	1875 2450 1875 2050
Wire Bus Line
	2100 1100 7150 1100
Wire Bus Line
	4175 1100 4175 2650
Wire Bus Line
	1875 2050 2100 2050
Wire Wire Line
	6700 2350 6775 2350
Wire Wire Line
	6775 2350 6775 1650
Wire Wire Line
	4600 1650 4600 2150
Wire Wire Line
	4600 2150 4900 2150
Wire Wire Line
	6700 1950 7050 1950
Wire Wire Line
	6700 2050 7050 2050
Wire Wire Line
	6850 2550 6700 2550
Wire Wire Line
	6850 2850 6850 2950
Connection ~ 4775 1950
Wire Wire Line
	4300 1950 4300 1850
Wire Wire Line
	4300 1850 4600 1850
Connection ~ 4600 1850
Wire Wire Line
	4300 2250 4475 2250
Wire Wire Line
	8650 1350 9025 1350
Connection ~ 8850 1350
Wire Wire Line
	7625 1650 9025 1650
Connection ~ 8250 1650
Wire Wire Line
	9025 1450 9025 1725
Connection ~ 8850 1650
Connection ~ 7625 1350
Connection ~ 6775 1350
Connection ~ 9025 1650
Wire Wire Line
	8925 2225 9025 2225
Wire Wire Line
	9025 2125 9025 2350
Connection ~ 9025 2225
Wire Wire Line
	4300 2250 4300 3050
Wire Wire Line
	4200 3050 3475 3050
Wire Wire Line
	3475 3450 3475 3550
Wire Wire Line
	3175 3250 3075 3250
Wire Wire Line
	2775 3250 2200 3250
Wire Wire Line
	7475 2050 7250 2050
Wire Wire Line
	6775 2150 7475 2150
Connection ~ 6775 2150
Wire Bus Line
	7150 1100 7150 1950
Wire Wire Line
	9025 2025 8625 2025
Wire Wire Line
	8625 2550 9025 2550
Wire Wire Line
	9025 2550 9025 2450
Wire Wire Line
	8625 2425 8925 2425
Wire Wire Line
	8925 2425 8925 2350
Wire Wire Line
	8925 2350 9025 2350
Wire Wire Line
	8625 2950 8625 3050
Wire Wire Line
	8325 2225 8325 2350
Wire Wire Line
	8325 2525 8325 2750
$Comp
L R R9
U 1 1 573A8F76
P 7850 2100
F 0 "R9" V 7930 2100 50  0000 C CNN
F 1 "10k" V 7850 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 7780 2100 50  0001 C CNN
F 3 "" H 7850 2100 50  0000 C CNN
	1    7850 2100
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 573A9023
P 8025 2100
F 0 "R10" V 8105 2100 50  0000 C CNN
F 1 "10k" V 8025 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 7955 2100 50  0001 C CNN
F 3 "" H 8025 2100 50  0000 C CNN
	1    8025 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1950 8025 1950
Wire Wire Line
	7500 1350 7500 1750
Wire Wire Line
	7500 1750 7850 1750
Wire Wire Line
	7850 1750 7850 1950
Connection ~ 7500 1350
Wire Wire Line
	8025 2250 8025 2350
Wire Wire Line
	7850 2250 7850 2525
Wire Wire Line
	7625 2525 8025 2525
$Comp
L LED D1
U 1 1 573A9A42
P 5600 1200
F 0 "D1" H 5600 1300 50  0000 C CNN
F 1 "LED" H 5600 1100 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 5600 1200 50  0001 C CNN
F 3 "" H 5600 1200 50  0000 C CNN
	1    5600 1200
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 573AA3AA
P 5125 1200
F 0 "R6" V 5205 1200 50  0000 C CNN
F 1 "220" V 5125 1200 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 5055 1200 50  0001 C CNN
F 3 "" H 5125 1200 50  0000 C CNN
	1    5125 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 1200 5275 1200
Wire Wire Line
	5800 1200 5800 1350
Wire Wire Line
	4975 1200 4850 1200
Wire Wire Line
	8025 2350 7625 2350
Connection ~ 7850 2525
Text Label 7625 2350 0    60   ~ 0
MB_RESET
Text Label 7625 2525 0    60   ~ 0
MB_POWER
Wire Wire Line
	4900 2450 4600 2450
Wire Wire Line
	4600 2550 4900 2550
Text Label 4600 2450 0    60   ~ 0
MB_RESET
Text Label 4600 2550 0    60   ~ 0
MB_POWER
$Comp
L PWR_FLAG #FLG09
U 1 1 573AC34F
P 8850 1175
F 0 "#FLG09" H 8850 1270 50  0001 C CNN
F 1 "PWR_FLAG" H 8850 1355 50  0000 C CNN
F 2 "" H 8850 1175 50  0000 C CNN
F 3 "" H 8850 1175 50  0000 C CNN
	1    8850 1175
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 1175 8850 1350
$Comp
L PWR_FLAG #FLG010
U 1 1 573AC446
P 8850 725
F 0 "#FLG010" H 8850 820 50  0001 C CNN
F 1 "PWR_FLAG" H 8850 905 50  0000 C CNN
F 2 "" H 8850 725 50  0000 C CNN
F 3 "" H 8850 725 50  0000 C CNN
	1    8850 725 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 573AC49A
P 8850 800
F 0 "#PWR011" H 8850 550 50  0001 C CNN
F 1 "GND" H 8975 750 50  0000 C CNN
F 2 "" H 8850 800 50  0000 C CNN
F 3 "" H 8850 800 50  0000 C CNN
	1    8850 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 725  8850 800 
$Comp
L DB9_SHIELD J1
U 1 1 573A4B0A
P 1075 2350
F 0 "J1" H 1075 2900 50  0000 C CNN
F 1 "DB9_SHIELD" H 1075 1800 50  0000 C CNN
F 2 "hetii:DB9FC" H 1075 2350 50  0001 C CNN
F 3 "" H 1075 2350 50  0000 C CNN
	1    1075 2350
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR012
U 1 1 573A4ECE
P 725 2450
F 0 "#PWR012" H 725 2200 50  0001 C CNN
F 1 "GND" H 725 2300 50  0000 C CNN
F 2 "" H 725 2450 50  0000 C CNN
F 3 "" H 725 2450 50  0000 C CNN
	1    725  2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	725  2350 725  2450
$Comp
L CONN_01X01 P6
U 1 1 573A54F3
P 7050 2450
F 0 "P6" H 7050 2550 50  0000 C CNN
F 1 "1wire" H 7225 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 7050 2450 50  0001 C CNN
F 3 "" H 7050 2450 50  0000 C CNN
	1    7050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2450 6850 2450
NoConn ~ 4900 2250
Wire Wire Line
	4850 1200 4850 2350
Wire Wire Line
	4850 2350 4900 2350
$EndSCHEMATC
